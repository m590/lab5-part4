const mongoose = require('mongoose');
const Student = mongoose.model("Student");


module.exports.getAllCourses = function (req, res) {
    const studentId = req.params.studentId;
    Student.findById(studentId).exec(function (err, student) {
        const response = {
            status: 200,
            content: student.courses
        };

        if (err) {
            console.log('Error get games', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!student) {
            response.status = 400;
            response.content = {content: 'Student not found'};
        }
        res.status(response.status).json(response.content);
    });
};

module.exports.getOneCourse = function (req, res) {
    const studentId = req.params.studentId;
    const courseId = req.params.courseId;

    Student.findById(studentId).select('courses').exec(function (err, student) {
        const response = {
            status: 200
        };

        if (err) {
            console.log('Error get course', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!student) {
            response.status = 400;
            response.content = {content: 'Student not found'};
        }
        if (response.status == 200) {
            console.log('student', student);
            response.content = student.courses.id(courseId);
        }
        res.status(response.status).json(response.content);
    });
};

const _saveCourse = function (req, res, student) {
    student.courses.push({
        name: req.body.name,
        code: req.body.code
    });

    student.save(function (err, savedStudent) {
        const response = {
            status: 201,
            content: savedStudent
        };

        if (err) {
            console.log('Error get games', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.addCourse = function (req, res) {
    const studentId = req.params.studentId;

    Student.findById(studentId).exec(function (err, student) {
        const response = {
            status: 200
        };

        if (err) {
            console.log('Error get course', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!student) {
            response.status = 400;
            response.content = {content: 'Student not found'};
        }
        if (response.status == 200) {
            _saveCourse(req, res, student);
        } else {
            res.status(response.status).json(response.content);
        }
    });
};

const _courseUpdate = function (req, res, student) {
    const courseId = req.params.courseId;
    for (let i = 0; i < student.courses.length; i++) {
        if (student.courses[i]._id == courseId) {
            student.courses[i].name = req.body.name;
            student.courses[i].code = req.body.code;
            break;
        }
    }

    student.save(function (err, savedStudent) {
        const response = {
            status: 201,
            content: savedStudent
        };

        if (err) {
            console.log('Error update course', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.updateCourse = function (req, res) {
    const studentId = req.params.studentId;

    Student.findById(studentId).exec(function (err, student) {
        const response = {
            status: 200
        };

        if (err) {
            console.log('Error get course', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!student) {
            response.status = 400;
            response.content = {content: 'Student not found'};
        }
        if (response.status == 200) {
            _courseUpdate(req, res, student);
        } else {
            res.status(response.status).json(response.content);
        }
    });
};

const _coursePartialUpdate = function (req, res, student) {
    const courseId = req.params.courseId;
    for (let i = 0; i < student.courses.length; i++) {
        if (student.courses[i]._id == courseId) {
            if (req.body.name) {
                student.courses[i].name = req.body.name;
            }
            if (req.body.code) {
                student.courses[i].code = req.body.code;
            }
            break;
        }
    }

    student.save(function (err, savedStudent) {
        const response = {
            status: 201,
            content: savedStudent
        };

        if (err) {
            console.log('Error update course', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.updatePartialCourse = function (req, res) {
    const studentId = req.params.studentId;

    Student.findById(studentId).exec(function (err, student) {
        const response = {
            status: 200
        };

        if (err) {
            console.log('Error get course', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!student) {
            response.status = 400;
            response.content = {content: 'Student not found'};
        }
        if (response.status == 200) {
            _coursePartialUpdate(req, res, student);
        } else {
            res.status(response.status).json(response.content);
        }
    });
};


const _deleteUpdate = function (req, res, student) {
    const courseId = req.params.courseId;
    for (let i = 0; i < student.courses.length; i++) {
        if (student.courses[i]._id == courseId) {
            student.courses[i] = {};
            break;
        }
    }

    student.save(function (err, savedStudent) {
        const response = {
            status: 201,
            content: savedStudent
        };

        if (err) {
            console.log('Error update course', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        }
        res.status(response.status).json(response.content);
    });
}


module.exports.deleteCourse = function (req, res) {
    const studentId = req.params.studentId;

    Student.findById(studentId).exec(function (err, student) {
        const response = {
            status: 200
        };

        if (err) {
            console.log('Error get course', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!student) {
            response.status = 400;
            response.content = {content: 'Student not found'};
        }
        if (response.status == 200) {
            _deleteUpdate(req, res, student);
        } else {
            res.status(response.status).json(response.content);
        }
    });
};