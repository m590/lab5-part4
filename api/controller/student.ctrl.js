const mongoose = require('mongoose');
const Student = mongoose.model("Student");

module.exports.getAllStudents = function (req, res) {
    let count = 1;
    if (req.query && req.query.count) {
        count = parseInt(req.query.count);
    }
    let offset = 0;
    if (req.query && req.query.offset) {
        offset = parseInt(req.query.offset);
    }
    if (isNaN(count) || isNaN(offset)) {
        res.status(400).json({content: 'Query parameter count and offset should be numbers.'});
        return;
    }

    Student.find().skip(offset).limit(count).exec(function (err, students) {
        const response = {
            status: 200,
            content: students
        };

        if (err) {
            console.log('Error get students', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else {
            res.status(response.status).json(response.content);
        }
    });

}

module.exports.getOneStudent = function (req, res) {
    const studentId = req.params.studentId;
    Student.findById(studentId).exec(function (err, student) {
        const response = {
            status: 200,
            content: student
        };

        if (err) {
            console.log('Error get games', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!student) {
            response.status = 400;
            response.content = {content: 'Student not found'};
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.addStudent = function (req, res) {
    const newStudent = {
        name: req.body.name,
        gpa: parseFloat(req.body.gpa)
    };
    console.log('newStudent', newStudent);

    Student.create(newStudent, function (err, student) {
        const response = {
            status: 201,
            content: student
        };

        if (err) {
            console.log('Error get games', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.fullUpdateStudent = function (req, res) {
    const studentId = req.params.studentId;

    const query = {
        $set: {
            name: req.body.name,
            gpa: parseFloat(req.body.gpa)
        }
    }

    Student.updateOne({_id: mongoose.Types.ObjectId(studentId)}, query, {}, function (err, student) {
        const response = {
            status: 200,
            content: student
        };

        if (err) {
            console.log('Error get games', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!student) {
            response.status = 400;
            response.content = {content: 'Student not found'};
        }
        res.status(response.status).json(response.content);
    })

}

module.exports.partialUpdateStudent = function (req, res) {
    const studentId = req.params.studentId;
    const query = {
        $set: {}
    }

    if (req.body.name) {
        query.$set.name = req.body.name;
    }
    if (req.body.gpa) {
        query.$set.gpa = parseFloat(req.body.gpa);
    }

    Student.updateOne({_id: mongoose.Types.ObjectId(studentId)}, query, {}, function (err, student) {
        const response = {
            status: 200,
            content: student
        };

        if (err) {
            console.log('Error get games', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!student) {
            response.status = 400;
            response.content = {content: 'Student not found'};
        }
        res.status(response.status).json(response.content);
    })
}

module.exports.deleteStudent = function (req, res) {
    const studentId = req.params.studentId;
    Student.deleteOne({_id: mongoose.Types.ObjectId(studentId)},function (err, student) {
        const response = {
            status: 204,
            content: student
        };

        if (err) {
            console.log('Error get games', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!student) {
            response.status = 400;
            response.content = {content: 'Student not found'};
        }
        res.status(response.status).json(response.content);
    })
}
