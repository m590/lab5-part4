const mongoose = require('mongoose');
require('./student.model');

const url = process.env.db_url + process.env.db_name_school;

console.log('url', url);

mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: true, useCreateIndex: true}, function () {
    console.log('school db connected');
});
