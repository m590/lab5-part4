const mongoose = require('mongoose');

const courseModel = mongoose.Schema({
    name: {
        type: String,
    },
    code: {
        type: String,
    },
    courseId: Number
});

const studentModel = mongoose.Schema({
    studentId: {
        type: Number,
    },
    name: {
        type: String,
        required: true
    },
    gpa: Number,
    courses: [courseModel]
});

mongoose.model("Student", studentModel, "students");