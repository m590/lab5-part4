const express = require('express');
const studentCtrl = require('../controller/student.ctrl');
const courseCtrl = require('../controller/course.ctrl');

const router = express.Router();

router.route("/students")
    .get(studentCtrl.getAllStudents)
    .post(studentCtrl.addStudent);

router.route("/students/:studentId")
    .get(studentCtrl.getOneStudent)
    .put(studentCtrl.fullUpdateStudent)
    .patch(studentCtrl.partialUpdateStudent)
    .delete(studentCtrl.deleteStudent);

router.route("/students/:studentId/courses")
    .get(courseCtrl.getAllCourses)
    .post(courseCtrl.addCourse);

router.route("/students/:studentId/courses/:courseId")
    .get(courseCtrl.getOneCourse)
    .put(courseCtrl.updateCourse)
    .patch(courseCtrl.updatePartialCourse)
    .delete(courseCtrl.deleteCourse);

module.exports = router;